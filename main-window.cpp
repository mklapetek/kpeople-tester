/*

    Copyright (C) 2012  Martin Klapetek <martin.klapetek@gmail.com>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/


#include "main-window.h"
#include "../Telepathy/kpeople/src/kpeople-manager.h"

#include <QTreeView>
#include <QHBoxLayout>

#include <kpeople/persons-model.h>
#include <kpeople/person-cache.h>
#include <KJob>
// #include <nepomuk/datamanagement.h>
#include <Nepomuk2/Vocabulary/PIMO>
// #include <nepomuk/createresourcejob.h>
#include <KDebug>
#include <QTimer>
#include <Soprano/Vocabulary/NAO>
#include <Nepomuk2/Vocabulary/NCO>

MainWindow::MainWindow(QWidget *parent)
    : KMainWindow(parent)
{
    QHBoxLayout *l = new QHBoxLayout(this);
//     centralWidget()->setLayout(l);
    QTreeView *v = new QTreeView(this);
    v->setIconSize(QSize(32, 32));
    setCentralWidget(v);

    //connect(v, SIGNAL(clicked(QModelIndex)),
    //        this, SLOT(clicked(QModelIndex)));
    QList<QUrl> list;
    list << Soprano::Vocabulary::NAO::prefLabel()
                        << Soprano::Vocabulary::NAO::prefSymbol()
                        << Nepomuk2::Vocabulary::NCO::imNickname()
                        << Nepomuk2::Vocabulary::NCO::imAccountType()
                        << Nepomuk2::Vocabulary::NCO::imID()
                        //                      << Nepomuk2::Vocabulary::Telepathy::statusType()
                        //                      << Nepomuk2::Vocabulary::Telepathy::accountIdentifier()
                        << QUrl(QLatin1String("http://nepomuk.kde.org/ontologies/2009/06/20/telepathy#statusType"))
                        << QUrl(QLatin1String("http://nepomuk.kde.org/ontologies/2009/06/20/telepathy#accountIdentifier"))
                        << Nepomuk2::Vocabulary::NCO::imStatus()
                        << Nepomuk2::Vocabulary::NCO::hasEmailAddress();

    QString query = QLatin1String("select distinct ?uri ?nao_prefLabel ?pimo_groundingOccurrence ?nco_hasIMAccount"
                                    " ?nco_imNickname ?telepathy_statusType ?nco_imID ?nco_imAccountType ?nco_hasEmailAddress"
                                    " ?nao_prefSymbol ?telepathy_accountIdentifier ?nco_imStatus "

                                    "WHERE { ?uri a pimo:Person ."

                                    "?uri                       pimo:groundingOccurrence    ?pimo_groundingOccurrence ."
                                    "?pimo_groundingOccurrence  nco:hasIMAccount            ?nco_hasIMAccount ."
                                    "?nco_hasIMAccount          nco:imNickname              ?nco_imNickname ."
                                    "?nco_hasIMAccount          telepathy:statusType        ?telepathy_statusType ."
                                    "?nco_hasIMAccount          nco:imStatus                ?nco_imStatus ."
                                    "?nco_hasIMAccount          nco:imID                    ?nco_imID . "
                                    "?nco_hasIMAccount          nco:imAccountType           ?nco_imAccountType ."
                                    "?nco_hasIMAccount          nco:isAccessedBy            ?nco_isAccessedBy ."
                                    "?nco_isAccessedBy          telepathy:accountIdentifier ?telepathy_accountIdentifier ."

                                    "OPTIONAL { ?uri                       nao:prefLabel        ?nao_prefLabel . }"
                                    "OPTIONAL { ?uri                       nao:prefSymbol       ?nao_prefSymbol . }"
                                    "OPTIONAL { ?pimo_groundingOccurrence  nco:hasEmailAddress  ?nco_hasEmailAddress . }"

                                    "}");

    KPeopleManager *m = new KPeopleManager(PersonCache::instance());
    m->setQuery(query);
    m->setTerms(list);
    m->startQuery();
    v->setModel(m->model());

//     QTimer::singleShot(4000, this, SLOT(test()));
}

MainWindow::~MainWindow()
{
}

void MainWindow::clicked(const QModelIndex &index)
{
//     cd.startChat(index.data(IMPersonsModel::AccountIdRole).toString(), index.data(IMPersonsModel::ContactIdRole).toString());
}


void MainWindow::test()
{
//     Nepomuk::CreateResourceJob *job = Nepomuk::createResource(QList<QUrl>() << Nepomuk::Vocabulary::PIMO::Person(),
//                                         "xxTest person 3",
//                                         QString());
//     job->exec();
//     kDebug() << job->errorString();
}
