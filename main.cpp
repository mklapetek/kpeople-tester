#include <KApplication>
#include <KDebug>
#include <KCmdLineArgs>
#include <KAboutData>

#include "main-window.h"

int main(int argc, char **argv) {
    KAboutData aboutData("person-viewer", 0, ki18n("PIMO:Person Viewer"), "0.1",
                         ki18n("PIMO:Person Viewer"), KAboutData::License_GPL_V2,
                         ki18n("(C) 2011, Martin Klapetek"));
    KCmdLineArgs::init(argc, argv, &aboutData);


    KApplication app;

    MainWindow *w = new MainWindow(0);
    w->show();

    app.exec();

    return 0;
}
